package string3.gHappy;
// We'll say that a lowercase 'g' in a string is "happy" if there is another 'g'
// immediately to its left or right. Return true if all the g's in the given string are happy.


public class TestGHappy {
    public static void main(String[] args) {
        System.out.println("gHappy(\"xxgggxyg\") -> " + gHappy("xxgggxyg"));
        System.out.println("gHappy(\"gg\") -> " + gHappy("gg"));
        System.out.println("gHappy(\"mgm\") -> " + gHappy("mgm"));
        System.out.println("gHappy(\"xxgggxygg\") -> " + gHappy("xxgggxygg"));
    }

    public static boolean gHappy(String str) {
        boolean result = false;

        if (str.length() == 0) {
            return true;
        }

        if (str.length() == 1) {
            return false;
        }

        // Находим позицию первого символа 'g'
        int indexG = str.indexOf('g');

        // Проверяем его соседей, если условие не выполняется
        // сразу возвращаем false
        // иначе, продолжаем до конца строки
        while (indexG >= 0) {
            // Крайний случай 'g' в начале строки
            if (indexG == 0) {
                if (str.charAt(indexG + 1) == 'g') {
                    result = true;
                } else {
                    return false;
                }
            }
            // Крайний случай 'g' в конце строки
            if (indexG == str.length() - 1) {
                if (str.charAt(indexG - 1) == 'g') {
                    result = true;
                } else {
                    return false;
                }
            }
            // Общий случай 'g' в середине строки
            if (indexG > 0 && indexG < str.length() - 1) {
                if (str.charAt(indexG + 1) == 'g' || str.charAt(indexG - 1) == 'g') {
                    result = true;
                } else {
                    return false;
                }
            }
            indexG = str.indexOf('g', indexG + 1);
        }

        return result;
    }
}
