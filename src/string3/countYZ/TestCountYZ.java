package string3.countYZ;
// Given a string, count the number of words ending in 'y' or 'z' -- so the 'y' in "heavy"
// and the 'z' in "fez" count,// but not the 'y' in "yellow" (not case sensitive).
// We'll say that a y or z is at the end of a word if there is not
// an alphabetic letter immediately following it.
// (Note: Character.isLetter(char) tests if a char is an alphabetic letter.)

public class TestCountYZ {
    public static void main(String[] args) {
        System.out.println("countYZ(\"fez day\") -> " + countYZ("fez day"));
        System.out.println("countYZ(\"DAY abc XYZ\") -> " + countYZ("DAY abc XYZ"));
        System.out.println("countYZ(\"!!day--yaz!!\") -> " + countYZ("!!day--yaz!!"));
        System.out.println("countYZ(\"zxyx\") -> " + countYZ("zxyx"));
    }

    public static int countYZ(String str) {
        // Разбиваем строку на массив
        String[] arrStrings = str.split("[^A-Za-z]");
        int count = 0;

        // Считаем слова, которые заканчиваются на 'y' или 'z'
        for (String arrStr : arrStrings) {
            if (arrStr.toLowerCase().endsWith("y") || arrStr.toLowerCase().endsWith("z")) {
                count++;
            }
        }
        return count;
    }
}
