package string3.sameEnds;
// Given a string, return the longest substring that appears at both
// the beginning and end of the string without overlapping.
// For example, sameEnds("abXab") is "ab".

public class TestSameEnds {
    public static void main(String[] args) {
        System.out.println("sameEnds(\"abXYab\") -> " + sameEnds("abXYab"));
        System.out.println("sameEnds(\"javajava\") -> " + sameEnds("javajava"));
        System.out.println("sameEnds(\"Hello! and Hello!\") -> " + sameEnds("Hello! and Hello!"));
        System.out.println("sameEnds(\"x\") -> " + sameEnds("x"));
    }

    public static String sameEnds(String string) {
        // Находим середину строки
        int firstIndex = string.length()/2;
        int secondIndex = string.length()/2;

        // Если в строке нечетное количество символов
        // исключаем символ в середине
        if(string.length()%2 == 1){
            secondIndex++;
        }

        // Разбиваем исходную строку на две и сравниваем,
        // до тех пор пока нечего будет сравнивать
        while (firstIndex >= 0) {
            String firstString = string.substring(0, firstIndex);
            String secondString = string.substring(secondIndex, string.length());
            if (firstString.equals(secondString)) {
                return firstString;
            }
            firstIndex--;
            secondIndex++;
        }
        return "";
    }
}
