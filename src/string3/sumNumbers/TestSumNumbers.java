package string3.sumNumbers;
// Given a string, return the sum of the numbers appearing in the string, ignoring all other
// characters. A number is a series of 1 or more digit chars in a row. (Note:
// Character.isDigit(char) tests if a char is one of the chars '0', '1', .. '9'.
// Integer.parseInt(string) converts a string to an int.)


public class TestSumNumbers {
    public static void main(String[] args) {
        System.out.println("sumNumbers(\"abc123xyz\") -> " + sumNumbers("abc123abc"));
        System.out.println("sumNumbers(\"aa11b33\") -> " + sumNumbers("aa11bb33"));
        System.out.println("sumNumbers(\"7 11\") -> " + sumNumbers("7 11"));
        System.out.println("sumNumbers(\"Chocolate\") -> " + sumNumbers("Chocolate"));
    }

    public static int sumNumbers(String str) {
        // Возвращаемое значение
        int sum = 0;
        // Показывает было ли ранее найдено число или нет
        boolean findNumber = false;
        // Индекс начала числа
        int begin = 0;

        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            // Если нашли цифру, а до этого их не было
            // запоминаем индекс начала числа и ставим
            // findNumber в true
            if (Character.isDigit(ch) && !findNumber) {
                findNumber = true;
                begin = i;
            }
            // Если нашли символ, не являющийся цифрой
            // проверяем были ли цифры до него, если да
            // парсим наше число в sum и сбрасываем findNumber
            if (!Character.isDigit(ch) && findNumber) {
                sum += Integer.parseInt(str.substring(begin, i));
                findNumber = false;
            }
            // Если дошли до конца строки, а findNumber все еще в true
            // значит у нас есть еще одно последнее число
            // парсим его в sum
            if (i == str.length() - 1 && findNumber) {
                sum += Integer.parseInt(str.substring(begin, str.length()));
            }
        }

        return sum;
    }
}
