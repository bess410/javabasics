package array3.canBalance;
// Given a non-empty array, return true if there is a place to split the array so that the sum
// of the numbers on one side is equal to the sum of the numbers on the other side.


public class TestCanBalance {
    public static void main(String[] args) {
        System.out.println("canBalance([1, 2, 3, 1, 0, 1, 3]) - > " + canBalance(new int[]{1, 2, 3, 1, 0, 1, 3}));
        System.out.println("canBalance([1, 1, 1, 2, 1]) -> " + canBalance(new int[]{1, 1, 1, 2, 1}));
        System.out.println("canBalance([10, 10]) -> " + canBalance(new int[]{10, 10}));
        System.out.println("canBalance([1]) -> " + canBalance(new int[]{1}));
    }

    public static boolean canBalance(int[] nums) {
        // Для каждого значения массива, начиная с первого
        // Проверяем есть ли у нас баланс или нет
        for (int i = 1; i < nums.length; i++) {
            int sumLeft = getSumOfRangeArr(nums, 0, i);
            int sumRight = getSumOfRangeArr(nums, i, nums.length);
            if (sumLeft == sumRight) {
                return true;
            }
        }
        return false;
    }

    // Считает сумму чисел в заданном диапазоне массива
    private static int getSumOfRangeArr(int[] arrInts, int begin, int end) {
        int sum = 0;
        for (int i = begin; i < end; i++) {
            sum += arrInts[i];
        }
        return sum;
    }
}
