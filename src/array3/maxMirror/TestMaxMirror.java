package array3.maxMirror;
// We'll say that a "mirror" section in an array is a group of contiguous elements such that
// somewhere in the array, the same group appears in reverse order. For example, the
// largest mirror section in {1, 2, 3, 8, 9, 3, 2, 1} is length 3 (the {1, 2, 3} part). Return the
// size of the largest mirror section found in the given array.

public class TestMaxMirror {
    public static void main(String[] args) {
        System.out.println("maxMirror([1, 2, 3, 8, 9, 3, 2, 1]) -> " + maxMirror(new int[]{1, 2, 3, 8, 9, 3, 2, 1}));
        System.out.println("maxMirror([]) -> " + maxMirror(new int[]{}));
        System.out.println("maxMirror([1, 2, 1, 20, 21, 1, 2, 1, 2, 23, 24, 2, 1, 2, 1, 25]) -> " +
                maxMirror(new int[]{1, 2, 1, 20, 21, 1, 2, 1, 2, 23, 24, 2, 1, 2, 1, 25}));
        System.out.println("maxMirror([1, 2, 3, 2, 1]]) -> " + maxMirror(new int[]{1, 2, 3, 2, 1}));
    }

    public static int maxMirror(int[] nums) {
        if (nums.length == 1) {
            return 1;
        }
        // Первоначальная максимальная длина зеркала
        int maxMirror = 0;

        // Для каждого значения в массиве проверяем есть ли у него зеркало
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                int mirror = getMirror(nums, i, j);
                // Ищем максимальную длину зеркала
                maxMirror = mirror > maxMirror ? mirror : maxMirror;
            }
        }
        return maxMirror;
    }

    // Возвращает длину зеркала, если оно есть
    private static int getMirror(int[] nums, int begin, int end) {
        int mirror = 0; // длина нашего зеркала

        if (nums[begin] == nums[end]) {

            // Проверяем длину зеркала
            while (begin < end) {
                // Если значения совпадают, увечичиваем длину
                // если нет возвращаем длину
                if (nums[begin] == nums[end]) {
                    mirror++;
                } else {
                    break;
                }
                // Если значения стоят рядом [1, 2, 3, 3, 2, 1], удваиваем длину
                if (begin == end - 1) {
                    mirror *= 2;
                }
                // Если значения стоят через одно значение [1, 2, 3, 2, 1]
                // удваиваем длину и прибавляем еще одно значение в середине
                if (begin == end - 2) {
                    mirror = mirror * 2 + 1;
                }

                // Двигаемся по массиву с разных сторон
                begin++;
                end--;
            }
        }
        return mirror;
    }
}
