package array3.maxSpan;
// Consider the leftmost and righmost appearances of some value in an array. We'll say that
// the "span" is the number of elements between the two inclusive. A single value has a span
// of 1. Returns the largest span found in the given array. (Efficiency is not a priority.)


public class TestMaxSpan {
    public static void main(String[] args) {
        System.out.println("maxSpan([1, 2, 1, 1, 3]) -> " + maxSpan(new int[]{1, 2, 1, 1, 3}));
        System.out.println("maxSpan([]) -> " + maxSpan(new int[]{}));
        System.out.println("maxSpan([3, 9, 9]) -> " + maxSpan(new int[]{3, 9, 9}));
        System.out.println("maxSpan([1, 4, 2, 1, 4, 4, 4]) -> " + maxSpan(new int[]{1, 4, 2, 1, 4, 4, 4}));
    }

    public static int maxSpan(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        int maxSpan = 1;
        int tempMax;
        // Проходимся по массиву и для каждого его значения,
        // ищем такие же.
        for (int i = 0; i < nums.length - 1; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                // Если находим одинаковые значения
                if (nums[i] == nums[j]) {
                    // Вычисляем диапазон между ними
                    tempMax = j - i + 1;
                    // Находим максимальный диапазон
                    maxSpan = tempMax > maxSpan ? tempMax : maxSpan;
                }
            }
        }
        return maxSpan;
    }
}
