package array3.seriesUp;
// Given n>=0, create an array with the pattern {1,    1, 2,    1, 2, 3,   ... 1, 2, 3 .. n}
// (spaces added to show the grouping). Note that the length of the array will be 1 + 2 + 3
// ... + n, which is known to sum to exactly n*(n + 1)/2.


import java.util.Arrays;

public class TestSeriesUp {
    public static void main(String[] args) {
        System.out.println("seriesUp(0) -> " + Arrays.toString(seriesUp(0)));
        System.out.println("seriesUp(4) -> " + Arrays.toString(seriesUp(4)));
        System.out.println("seriesUp(2) -> " + Arrays.toString(seriesUp(2)));
        System.out.println("seriesUp(1) -> " + Arrays.toString(seriesUp(1)));
    }

    public static int[] seriesUp(int n) {
        int[] arrInts = new int[n * (n + 1) / 2];
        int count = 0;
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= i; j++) {
                arrInts[count++] = j;
            }
        }
        return arrInts;
    }
}
